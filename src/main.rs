#[allow(dead_code)]
#[macro_use]
extern crate bitfield;
use std::ops;

// #[derive(Debug, Clone)]
// #[derive(Clone)]
#[derive(Copy, Clone)]
struct FloatBits(u32);
bitfield_bitrange! {struct FloatBits(u32)}

impl FloatBits {
    bitfield_fields! {
        // u32;
        // fraction, _: 23, 0;
        // exponent, _: 30, 24;
        // sign, _: 31;

        // u8, sign, _: 31;
        // u8, exponent, _: 30, 24;
        // u32, fraction, _: 23, 0;

        u8;
        // sign, _: 31; // boolean
        sign, _: 31, 31; // u8, 1 bit
        pub exponent, _: 30, 23; // 8 bits
        u32;
        pub fraction, _: 22, 0; // 23 bits
    }
}

impl std::fmt::Debug for FloatBits {
    bitfield_debug! {
        struct FloatBits;
        u8;
        sign, _: 0;
        exponent, _: 0;
        u32;
        fraction, _: 0;
    }
}

impl ops::Mul<IeeeFloat> for IeeeFloat {
    type Output = IeeeFloat;
    fn mul(self, b: IeeeFloat) -> Self::Output {
        let a = self;
        unsafe {
            let sign = ((a.u.sign() * b.u.sign()) as u32) << 31;
            let mut exponent = a.u.exponent() as u32 + b.u.exponent() as u32;
            let mut fraction =
                (((1 << 23) + a.u.fraction() as u64) * ((1 << 23) + b.u.fraction() as u64)) >> 23;

            // let bit 23 always be 1
            if fraction > (1 << 24) {
                while fraction > (1 << 24) {
                    fraction = fraction >> 1;
                    exponent += 1;
                }
            } else {
                while fraction < (1 << 23) {
                    fraction = fraction << 1;
                    exponent -= 1;
                }
            }

            // inf
            if exponent > 255 + 128 {
                exponent = 255;
                fraction = 0;
            } else if exponent >= 128 {
                exponent -= 128;
            }
            // 0
            else {
                exponent = 0;
                fraction = 0;
            }

            // -inf and +inf
            // underflow
            if exponent > 512 {
                exponent = 0;
                fraction = 0;
            } else if exponent > 256 {
                exponent = 255;
                fraction = 0;
            }

            fraction = fraction % 0x1000000; // 1u64 << 24

            IeeeFloat {
                u: FloatBits(sign + (exponent << 23) + fraction as u32),
            }
        }
    }
}

// impl pub const fn as_ptr(&self) -> *const t{}
#[derive(Copy, Clone)]
union IeeeFloat {
    u: FloatBits,
    f: f32,
}

// Regular IEEE representation of floating point
// bitdata Fp32 : f32 {
fn main() {
    let inf = f32::INFINITY;
    let neg_inf = f32::NEG_INFINITY;
    let nan = f32::NAN;

    println!("inf*2 = {}", inf * 2.0);
    println!("neg_inf*2 = {}", neg_inf * 2.0);

    println!("inf/2 = {}", inf / 2.0);
    println!("neg_inf/2 = {}", neg_inf / 2.0);

    println!("inf*inf = {}", inf * inf);
    println!("inf*neg_inf = {}", inf * neg_inf);

    println!("inf/inf = {}", inf / inf);
    println!("inf/neg_inf = {}", inf / neg_inf);

    println!("1.0/0 = {}", 1.0f32 / 0.0);
    println!("1.0/-0 = {}", 1.0f32 / -0.0);
    println!("-1.0/0 = {}", -1.0f32 / 0.0);
    println!("-1.0/-0 = {}", -1.0f32 / -0.0);

    println!("inf/neg_inf = {}", inf / neg_inf);

    // let u0 = FloatBits((1 << 31) + (0x7f << 23));
    let data: Vec<Vec<u32>> = vec![
        // vec![0, 0x7f, 0x000000], // 1
        vec![0, 0x7d, 0x000000], // 0.25
        vec![0, 0x7e, 0x000000], // 0.5
        vec![0, 0x80, 0x000000], // 2
        vec![0, 0x84, 0x200000], // 40
        vec![0, 0x80, 0x400000], // 3
        vec![1, 0x7f, 0],
        vec![0, 0x7f, 0],
        vec![0, 0x7f, 1],
        vec![0, 0x7e, 1],
        vec![0, 0x1, 1],
        vec![0, 0x0, 0x7fffff],
        vec![0, 0x0, 1],
    ];

    let mut v: Vec<IeeeFloat> = vec![];
    for i in &data {
        let u = IeeeFloat {
            u: FloatBits((i[0] << 31) + (i[1] << 23) + i[2]),
        };
        v.push(u);
    }

    unsafe {
        for _u in &v {
            println!("****************************************");
            if _u.f < 1e-5 || _u.f > 1e5 {
                println!("u = {:?},\nf = {:e}", _u.u, _u.f);
            } else {
                println!("u = {:?},\nf = {:.}", _u.u, _u.f);
            }
            println!(
                "sign = {}, exponent = {:#02x}, fraction = {:#08X}",
                _u.u.sign(),
                _u.u.exponent(),
                _u.u.fraction()
            );
        }
        println!("****************************************");
        println!("(v[0] * v[1]).f = {}", (v[0] * v[1]).f);
    }
}
